<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 17:11
 */

namespace Kernel;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;

/**
 * Абстрактный класс для наших консольных команд
 * @package App\Commands
 */
abstract class AbstractCommand extends Command
{
    /**
     * @var LoggerInterface Логирование событий
     */
    protected $logger;

    /**
     * Конструктор.
     * @param null|string $name Имя команды. Если передан null, то название будет полученно из метода configure
     *
     * @throws LogicException   В случае, если наш метод configure не вернет название команды
     */
    public function __construct(string $name = null)
    {
        parent::__construct($name);
        $this->logger = new Logger();
    }

}