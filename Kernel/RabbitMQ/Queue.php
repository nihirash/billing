<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 20:19
 */

namespace Kernel\RabbitMQ;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Очередь задач
 * @package Kernel\RabbitMQ
 */
class Queue extends Base
{
    /**
     * @var string  Имя очереди задач в рамках которой работаем
     */
    private $queueName;

    /**
     * Подключиться к очереди сообщений
     *
     * @param string $queue
     * @return $this
     */
    public function bindQueue(string $queue)
    {
        $this->queueName = $queue;
        $this->channel->queue_declare(
            $queue,     // Имя очереди
            false,      // Пассивный?
            true,       // Должна ли очередь пережить перезапуск брокера?
            false,      // Эксклюзивная ли очередь?
            false       // Удалять ли содержимое, если отпишется последний подписчик?
        );

        return $this;
    }

    /**
     * Подключить потребителя
     *
     * @param callable $callback Колбек, обрабатывающий получаемые сообщения
     * @return $this
     */
    public function attachConsumer(callable $callback)
    {
        $this->channel->basic_consume(
            $this->queueName,   // Имя очереди
            '',                 // От кого можем получать сообщения
            false,              // Не локальный
            false,               // Подтверждения
            false,              // Эксклюзивная
            false,              // Не ждать
            $callback           // Колбек принимающий сообщения
        );

        return $this;
    }

    /**
     * Опубликовать сообщение
     *
     * @param string $message
     */
    public function publish(string $message)
    {
        $amqpMessage = new AMQPMessage($message);
        $this->channel->basic_publish($amqpMessage, '', $this->queueName);
    }
}