<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 22:30
 */

namespace Kernel\RabbitMQ;

use App\Config;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class Base
{
    /**
     * @var AMQPStreamConnection    Соединение с RabbitMQ
     */
    protected $connection;
    /**
     * @var AMQPChannel     Канал
     */
    protected $channel;

    /**
     * Конструктор очереди
     */
    public function __construct()
    {
        $config = Config::getValue(Config::RABBITMQ);
        $this->connection = new AMQPStreamConnection(
            $config[Config::HOST],
            $config[Config::PORT],
            $config[Config::USER],
            $config[Config::PASSWORD]
        );

        $this->channel = $this->connection->channel();
    }

    /**
     * Запустить цикл потребителя
     */
    public function work()
    {
        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }

    /**
     * Получить канал
     *
     * @return AMQPChannel
     */
    public function getChannel() : AMQPChannel
    {
        return $this->channel;
    }

    /**
     * Закроем за собой подключения
     */
    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }
}