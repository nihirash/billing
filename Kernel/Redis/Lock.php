<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 17:43
 */

namespace Kernel\Redis;


class Lock
{
    const LOCK_PREFIX = 'lock@';    // Чтобы избежать случайного пересечения с нелоками

    /**
     * @var string  имя лока
     */
    private $lockName;
    /**
     * @var bool Владеем ли ресурсом?
     */
    private $owned = false;
    /**
     * @var Connection  Соединение с редисом
     */
    private $redisConnection;
    /**
     * @var int     Сколько живет лок, на случай падения скрипта
     */
    private $expire;

    /**
     * Создание объекта лока
     * @param string $name Имя лока
     * @param int $lockExpire Сколько лок проживет в секундах
     */
    public function __construct(string $name, $lockExpire = 5)
    {
        $this->lockName = self::LOCK_PREFIX . $name;
        $this->redisConnection = Connection::getInstance();
        $this->expire = $lockExpire;
    }

    /**
     * Снять лок - только если владеем им
     *
     * @return bool Успешность операции
     */
    public function unlock(): bool
    {
        if ($this->owned) {
            $this->redisConnection->del([$this->lockName]);
            return true;
        }
        return false;
    }

    /**
     * Попытаться дождаться разблокировки
     *
     * @param int $times Сколько шансов дадим себе
     * @return bool         Успех?
     */
    public function tryLock(int $times = 20): bool
    {
        for ($counter = 0; $counter < $times; $counter++) {
            if ($this->lock()) {
                return true;
            }
            usleep(500000); // Ждем пол секунды
        }
        return false;
    }

    /**
     * Установка лока
     *
     * @return bool Успешность операции
     */
    public function lock(): bool
    {
        // Инкремент - операция атомарная
        $lockCounter = $this->redisConnection->incr($this->lockName);

        if ($lockCounter > 1) {
            return false;
        }
        $this->redisConnection->expire($this->lockName, $this->expire);
        $this->owned = true;
        return true;
    }

}