<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 18:06
 */

namespace Kernel\Redis;


use App\Config;
use Kernel\Logger;
use Predis\Client;

class Connection extends Client
{
    /**
     * @var Connection Инстанс класса
     */
    private static $instance;
    /**
     * @var mixed Соединение с редисом
     */
    private $redis;
    /**
     * @var
     */
    private $logger;

    /**
     * Для получения лучше использовать Connection::getInstance(), чтобы минимизировать получение инстансов
     * клиентов редиса
     */
    public function __construct()
    {
        $this->logger = new Logger();
        parent::__construct(Config::getValue(Config::REDIS));
    }

    /**
     * @return Connection    Получить экземпляр класса
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Connection();
        }

        return self::$instance;
    }

    /**
     * Обертка над всеми вызовами в редис.
     * При ошибке работы с редисом продолжать работу абсолютно небезопасно - лучше умереть,
     * но  при этом логируем все
     *
     * @param string $commandID
     * @param array $arguments
     * @return mixed
     */
    public function __call($commandID, $arguments)
    {
        try {
            return parent::__call($commandID, $arguments);
        } catch (\Throwable $exception) {
            $this->logger->critical("Redis error: {$exception->getMessage()}");
            die();
        }
    }

}