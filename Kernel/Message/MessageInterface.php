<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 22:04
 */

namespace Kernel\Message;

use App\Exception\DataException;

/**
 * Интерфейс для сообщений
 *
 * @package Kernel
 */
interface MessageInterface
{
    /**
     * Создание объекта сообщения из массива
     *
     * @param array $data Массив с данными
     * @return MessageInterface
     */
    public static function fromArray(array $data): MessageInterface;

    /**
     * Создание объекта сообщения из строки
     *
     * @param string $data
     * @return MessageInterface
     */
    public static function fromString(string $data): MessageInterface;

    /**
     * @return string   Форматировать сообщение перед отправкой
     */
    public function toString(): string;

    /**
     * @return array    Получить сообщения
     */
    public function getData(): array;

    /**
     * Провалидировать пакет
     *
     * @throws DataException    В случае ошибки валидации
     */
    public function validate();

    /**
     * Получить поле из сообщения
     *
     * @param string $name
     * @return null
     */
    public function getField(string $name);


    /**
     * Установить значение поля
     *
     * @param string $name  Имя поля
     * @param mixed $value  Значение
     *
     * @return MessageInterface
     */
    public function setField(string $name, $value) : MessageInterface;
}