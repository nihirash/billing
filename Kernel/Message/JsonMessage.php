<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 22:08
 */

namespace Kernel\Message;


class JsonMessage implements MessageInterface
{

    protected $data = [];

    /**
     * Конструктор json-сообщения
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Создание объекта сообщения из массива
     *
     * @param array $data Массив с данными
     * @return MessageInterface
     */
    public static function fromArray(array $data): MessageInterface
    {
        return new static($data);
    }

    /**
     * Создание объекта сообщения из строки
     *
     * @param string $data
     * @return MessageInterface
     */
    public static function fromString(string $data): MessageInterface
    {
        return new static(json_decode($data, true));
    }

    /**
     * @return string   Форматировать сообщение перед отправкой
     */
    public function toString(): string
    {
        return json_encode($this->data);
    }

    /**
     * @return array    Получить сообщения
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Валидация. Заглушка
     */
    public function validate()
    {
        return;
    }

    /**
     * Получить поле из сообщения
     *
     * @param string $name
     * @return null
     */
    public function getField(string $name)
    {
        return $this->data[$name] ?? null;
    }

    /**
     * Установить значение поля
     *
     * @param string $name Имя поля
     * @param mixed $value Значение
     *
     * @return MessageInterface
     */
    public function setField(string $name, $value) : MessageInterface
    {
        $this->data[$name] = $value;
        return $this;
    }


}