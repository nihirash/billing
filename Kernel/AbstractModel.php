<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 15:25
 */

namespace Kernel;


abstract class AbstractModel
{
    /**
     * Сохранить модель
     */
    public function save()
    {
        $entityManager = Bootstrap::getEntityManager();
        $entityManager->persist($this);
        $entityManager->flush();
    }
}