<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 17:18
 */

namespace Kernel;


use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

/**
 * Класс логирования событий - логирует в файл и дублирует на экран
 * @package App\Kernel
 *
 * @property string $pathToStoreLogs
 * @property string $template
 */
class Logger extends AbstractLogger implements LoggerInterface
{
    /**
     * @var string Путь к файлу с логами
     */
    public $pathToStoreLogs;

    /**
     * @var string Шаблон для представления логов
     */
    public $template = "{date} {level}: {message} {context}";

    /**
     * Конструктор логера.
     *
     * Выставляем имя файла в котром будут лежать наши логи
     */
    public function __construct()
    {
        $this->pathToStoreLogs = BASE_PATH . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . date('d.m.Y');
    }

    /**
     * Логируем событие
     *
     * @param mixed $level Уровень события
     * @param string $message Сообщение, подлежащие логированию
     * @param array $context Контекст
     *
     * @return void
     */
    public function log($level, $message, array $context = array())
    {
        $logString = trim(
                strtr(
                    $this->template,
                    [
                        '{date}' => $this->getDate(),
                        '{level}' => strval($level),
                        '{message}' => strval($message),
                        '{context}' => $this->contextStringify($context)
                    ]
                )
            ) . PHP_EOL;

        // file_put_contents не является лучшим способом логирования, но в данном случае
        // эта реализация используется для примера, лишь бы не использовать NullLogger
        file_put_contents($this->pathToStoreLogs, $logString, FILE_APPEND);
        echo $logString;
    }

    /**
     * Получить строку с текущей датой для лога
     *
     * @return string
     */
    public function getDate(): string
    {
        return (new \DateTime())->format(\DateTime::RFC2822);
    }

    /**
     * Приводим контекст в строку - самый простой способ - в json
     *
     * @param array $context
     * @return string
     */
    public function contextStringify(array $context = []): string
    {
        return !empty($context) ? json_encode($context) : '';
    }
}