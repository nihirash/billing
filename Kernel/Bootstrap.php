<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 0:14
 */

namespace Kernel;


use App\Commands\SendTestRequests;
use App\Commands\Worker;
use App\Config;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\Console\Application;

/**
 * Инициализация фреймворка
 *
 * @package Kernel
 */
class Bootstrap
{
    /**
     * @var EntityManager   Менеджер сущностей Doctrine
     */
    private static $entityManager;

    /**
     * Инициализация
     */
    public function __construct()
    {
        $this->database();
    }

    /**
     * Инициализация БД
     */
    private function database()
    {
        $dbParams = Config::getValue(Config::DOCTRINE);
        $config = Setup::createAnnotationMetadataConfiguration(
            [BASE_PATH . DIRECTORY_SEPARATOR . 'Entity' . DIRECTORY_SEPARATOR],
            true
        );
        self::$entityManager = EntityManager::create($dbParams, $config);
    }

    /**
     * Получить EntityManager Doctrine
     *
     * @return EntityManager
     */
    public static function getEntityManager()
    {
        return self::$entityManager;
    }

    /**
     * Инициализация консольного приложения
     */
    public function consoleApp()
    {
        $this->migrateUp();
        $this->errorHandler();
        $application = new Application();
        $application->addCommands(
            [
                new Worker(),
                new SendTestRequests()
            ]);
        $application->run();
    }

    /**
     * Привести схему в порядок автоматом при запуске скрипта
     *
     * В реальном проекте я бы не стал выносить миграции/приведение схемы в автозагрузку
     */
    private function migrateUp()
    {
        exec('vendor/bin/doctrine orm:schema-tool:update --force');
    }

    /**
     * Обработка ошибок - пусть тоже валятся в логи
     */
    private function errorHandler()
    {
        set_error_handler(
            function ($errorNumber, $errorString, $errorFile, $errorLine, $errorContext) {
                // В случае неотловленного падения - откатим транзакцию
                $logger = new Logger();
                $logger->error("$errorNumber $errorFile $errorLine $errorString", $errorContext);
            });
    }
}