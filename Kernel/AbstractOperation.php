<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 15:40
 */

namespace Kernel;


use App\Exception\FinanceException;
use App\Exception\LogicException;
use App\Factory\ModelFactory;
use Entity\FinanceLog;
use Entity\UserBalance;
use Kernel\Redis\Lock;

/**
 * Базовый класс для финансовой операции
 *
 * @package Kernel
 */
abstract class AbstractOperation
{
    /**
     * userId для которого будет выполняться операция
     *
     * @var int
     */
    protected $userId;

    /**
     * Объект entity-менеджера доктрины
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;
    /**
     * @var Logger  Логирование событийы
     */
    protected $logger;
    /**
     * @var int     Количество денег в операции
     */
    protected $amount;
    /**
     * @var string Имя операции
     */
    protected $name;
    /**
     * @var FinanceLog  Лог о финансовом событии
     */
    protected $finLogModel;
    /**
     * @var UserBalance Баланс пользователя
     */
    protected $userBalanceModel;
    /**
     * @var bool    Операция на снятие денег?
     */
    protected $isNegative = false;
    /**
     * @var Lock
     */
    protected $lock;
    /**
     * @var int Какому пользователю перечисляют деньги
     */
    protected $toUserId;

    /**
     * @var int ID-запроса на заморозку средств
     */
    protected $requestId;

    /**
     * Инициализация общей бизнес-логики для сервисов
     */
    public function __construct()
    {
        $this->logger = new Logger();
        $this->entityManager = Bootstrap::getEntityManager();
        $this->entityManager->clear();
        gc_mem_caches();
    }

    /**
     * Установить userId для которого будет выполняться операция
     *
     * @param int $userId
     * @return $this
     * @throws FinanceException
     */
    public function setUserId(int $userId): AbstractOperation
    {
        if ($userId < 1) {
            throw new FinanceException('Попытка работать с пользователями, чей ID меньше нуля');
        }
        $this->userId = $userId;
        return $this;
    }

    /**
     * Установить количество денег, которое будет оперировать
     *
     * @param int $amount
     * @return $this
     * @throws FinanceException
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Выставить имя платежа
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Какому пользователю переводим деньги
     *
     * @param int $userId
     * @return $this
     */
    public function setToUserId(int $userId)
    {
        $this->toUserId = $userId;
        return $this;
    }

    /**
     * Установить номер запроса на заморозку средств
     *
     * @param int $requestId
     * @return $this
     */
    public function setRequestId(int $requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * Выполнить операцию
     *
     * @return array
     */
    abstract public function perform();

    /**
     * Начать операцию
     */
    protected function beginOperation()
    {
        $this->lock = new Lock('finance_' . $this->userId);

        if (!$this->lock->tryLock()) {
            throw new LogicException('Неудалось выставить блокировку на пользователя');
        }

        $this->logger->info("{$this->name} суммой {$this->amount} центов для пользователя {$this->userId} начато");
        $this->entityManager->beginTransaction();
        $this->userBalanceModel = ModelFactory::getUserBalanceModel($this->userId);

        $this->finLogModel = new FinanceLog();
        $this->finLogModel
            ->setAmount($this->isNegative ? -$this->amount : $this->amount)
            ->setBalanceBefore($this->userBalanceModel->getBalance())
            ->setUserBalanceModel($this->userBalanceModel)
            ->setCompleted(false)
            ->setLog($this->name)
            ->save();
    }

    /**
     * Закончить операцию
     */
    protected function finishOperation()
    {
        $this->finLogModel
            ->setCompleted(true)
            ->save();
        $this->entityManager->commit();
        $this->logger->info("$this->name суммой $this->amount центов для $this->userId завершено");
        $this->lock->unlock();
        return $this->finLogModel->getId();
    }

    /**
     * Отмена операции
     */
    protected function rollbackOperation()
    {
        $this->entityManager->rollback();
        $this->logger->error("$this->name суммой $this->amount центов для $this->userId отменено");
        $this->lock->unlock();
    }

}