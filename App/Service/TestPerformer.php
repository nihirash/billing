<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 13.09.17
 * Time: 0:01
 */

namespace App\Service;

use App\Config;
use App\Message\Task;
use Kernel\Logger;
use Kernel\RabbitMQ\Queue;
use PhpAmqpLib\Message\AMQPMessage;

class TestPerformer
{
    /**
     * @var Queue   Очередь для оправки задач
     */
    private $taskQueue;
    /**
     * @var string  имя очереди для получения ответов
     */
    private $responseQueueName;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * Инициализация очередей
     */
    private function initQueues()
    {
        $this->taskQueue = new Queue();
        $this->taskQueue->bindQueue(Config::getValue(Config::LISTEN_QUEUE));
    }

    /**
     * TestPerformer constructor.
     */
    public function __construct()
    {
        $this->logger = new Logger();
        $this->responseQueueName = 'test-' .  (time() + mt_rand(0,1000000));
    }

    /**
     * Отправить задачу на обработку
     *
     * @param Task $task
     */
    private function sendTask(Task $task, callable $callback)
    {
        $responseQueue = new Queue();
        $responseQueue->bindQueue($this->responseQueueName);
        $this->taskQueue->publish($task->toString());
        $responseQueue->attachConsumer($callback);
        $responseQueue->getChannel()->wait();
        $responseQueue = null;
        gc_mem_caches();
    }

    /**
     * Типовой колбек для тестов
     *
     * @param AMQPMessage $message
     */
    public function basicCallback(AMQPMessage $message)
    {
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        $response = json_decode($message->body, true);
        $this->logger->info('Операция ' . $response['result']['task']);
        if ($response['status'] == 'success') {
            $this->logger->info('Успешно выполнена');
            $this->logger->info('Результат: ' . json_encode($response['result']['result']));
        } else {
            $this->logger->info('Завершилась неудачей');
            $this->logger->info('Ошибка: ' . $response['result']['result']);
        }
    }

    public function freezeCallback(AMQPMessage $message)
    {
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        $response = json_decode($message->body, true);
        if ($response['status'] == 'success') {
            $this->logger->info('Успешно заморозили средства');
            return $response['result']['result']['freezeMoneyRequestId'];
        } else {
            $this->logger->info('Ошибка заморозки денег: ' . $response['result']['result']);
            return null;
        }
    }

    private function frozenMoneyTest()
    {
        $this->getBalanceTest();

        $freeze = new Task([
            'userId' => 1,
            'operation' => Config::FREEZE,
            'amount' => 10,
            'responseTo' => $this->responseQueueName
        ]);

        $unfreeze = new Task([
            'userId' => 1,
            'operation' => Config::UNBLOCK,
            'responseTo' => $this->responseQueueName,
        ]);
        $freezeID = null;

        $this->sendTask($freeze, function (AMQPMessage $message) use (&$freezeID) {
            $freezeID = $this->freezeCallback($message);
            return false;
        });

        $this->getBalanceTest();

        if ($freezeID) {
            $unfreeze->setField('requestId', $freezeID);
            $this->sendTask($unfreeze, [$this, 'basicCallback']);
        }
        $this->getBalanceTest();

        $this->sendTask($freeze, function (AMQPMessage $message) use (&$freezeID) {
            $freezeID = $this->freezeCallback($message);
            return false;
        });

        $this->getBalanceTest();

        if ($freezeID) {
            $unfreeze
                ->setField('requestId', $freezeID)
                ->setField('operation', Config::UNFREEZE);

            $this->sendTask($unfreeze, [$this, 'basicCallback']);
        }

        $this->getBalanceTest();
    }

    private function addMoneyTest()
    {
        $addMoney = new Task([
            'userId' => 1,
            'operation' => Config::ADD,
            'amount' => 300,
            'responseTo' => $this->responseQueueName
        ]);

        $this->sendTask($addMoney, [$this, 'basicCallback']);
    }

    private function drawMoneyTest()
    {
        $drawMoney = new Task([
            'userId' => 10,
            'operation' => Config::DRAW,
            'amount' => 100,
            'responseTo' => $this->responseQueueName
        ]);

        $this->sendTask($drawMoney, [$this, 'basicCallback']);
    }

    private function transferMoneyTest()
    {
        $transferMoney = new Task([
            'userId' => 1,
            'toUserId' => 10,
            'operation' => Config::TRANSFER,
            'amount' => 100,
            'responseTo' => $this->responseQueueName,
        ]);

        $this->sendTask($transferMoney, [$this, 'basicCallback']);
    }

    private function getBalanceTest()
    {
        $balance = new Task([
            'userId' => 1,
            'operation' => Config::GET,
            'responseTo' => $this->responseQueueName,
        ]);

        $this->sendTask($balance, [$this, 'basicCallback']);
    }

    /**
     * Выполнить интеграционные тесты(ставит задачи в очередь и получает ответы).
     */
    public function perform()
    {
        $this->logger->info('Начинаем тестирование');
        $this->initQueues();
        $this->getBalanceTest();
        $this->addMoneyTest();
        $this->drawMoneyTest();
        $this->transferMoneyTest();
        $this->frozenMoneyTest();
    }

}