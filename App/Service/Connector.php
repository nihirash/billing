<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 22:38
 */

namespace App\Service;

use App\Factory\OperationFactory;
use App\Message\Error;
use App\Message\Success;
use App\Message\Task;
use Kernel\Logger;
use Kernel\RabbitMQ\Queue;
use PhpAmqpLib\Message\AMQPMessage;

class Connector
{
    /**
     * Получаем сообщение
     *
     * @param AMQPMessage $message
     */
    public function messageArrival(AMQPMessage $message)
    {
        $logger = new Logger();
        $task = Task::fromString($message->body);

        try {
            $task->validate();
            $this->makeTask($task);
        } catch (\Throwable $exception) {
            $logger->error($exception->getMessage());
            self::responseError(
                $task,
                [
                    'task' => $message->body,
                    'result' => $exception->getMessage()
                ]);
        }

        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        return;
    }

    /**
     * Выполнить задачу
     *
     * @param Task $task
     */
    private function makeTask(Task $task)
    {
        $operation = OperationFactory::getOperationClass($task->getField('operation'));
        // Тут заполняем, в принципе, и не нужные нам поля тоже - хуже от этого не будет
        $operation
            ->setUserId((int)$task->getField('userId'))
            ->setAmount((int)$task->getField('amount'))
            ->setRequestId((int)$task->getField('requestId'))
            ->setToUserId((int)$task->getField('toUserId'));
        $result = $operation->perform();

        $this->responseSuccess(
            $task,
            [
                'task' => $task->toString(),
                'result' => $result
            ]);
    }

    /**
     * Ответить, что операция завершилась успешно
     *
     * @param $data
     */
    private function responseSuccess(Task $task, array $data)
    {
        $successMessage = Success::fromArray($data);
        $queue = new Queue();
        $queue
            ->bindQueue($task->getField('responseTo'))
            ->publish($successMessage->toString());
    }

    /**
     * Ответить, что случилась ошибка
     *
     * @param $data
     */
    private function responseError(Task $task, array $data)
    {
        if (!$task->getField('responseTo')) {
            /**
             *  Если некуда слать - пропускаем отправку ошибки,
             * но это актуально только для невалидных пакетов
             * Можно добавить дефолтную очередь для отправки сообщений(для невалидных пакетов)
             **/
            return;
        }

        $errorMessage = Error::fromArray($data);
        $queue = new Queue();
        $queue
            ->bindQueue($task->getField('responseTo'))
            ->publish($errorMessage->toString());
    }
}