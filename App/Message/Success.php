<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 22:17
 */

namespace App\Message;


use Kernel\Message\JsonMessage;

/**
 * Сообщение об успешном выполнении задачи
 *
 * @package App\Message
 */
class Success extends JsonMessage
{
    /**
     * Сериализация в JSON-строку
     *
     * @return string
     */
    public function toString(): string
    {
        $data = [
            'status' => 'success',
            'result' => $this->data
        ];
        return json_encode($data);
    }
}