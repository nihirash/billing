<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 22:14
 */

namespace App\Message;


use App\Exception\DataException;
use Kernel\Message\JsonMessage;

/**
 * Сообщение о задаче
 *
 * @package App\Message
 */
class Task extends JsonMessage
{
    /**
     * Валидировать пакет задачи
     *
     * @throws DataException
     */
    public function validate()
    {
        if (
            empty($this->data['operation']) ||
            empty($this->data['userId']) ||
            empty($this->data['responseTo']) ||
            !is_numeric($this->data['userId'])
        ) {
            throw new DataException('Пришел невалидный пакет');
        }
    }
}