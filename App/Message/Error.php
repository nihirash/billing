<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 22:15
 */

namespace App\Message;


use Kernel\Message\JsonMessage;

/**
 * Сообщение об ошибке
 *
 * @package App\Message
 */
class Error extends JsonMessage
{
    /**
     * Сериализация в JSON-строку
     *
     * @return string
     */
    public function toString(): string
    {
        $data = [
            'status' => 'error',
            'result' => $this->data
        ];
        return json_encode($data);
    }
}