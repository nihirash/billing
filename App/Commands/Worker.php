<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 17:32
 */

namespace App\Commands;

use App\Config;
use App\Service\Connector;
use Kernel\AbstractCommand;
use Kernel\RabbitMQ\Queue;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Класс основного исполнителя задач
 * @package App\Commands
 */
class Worker extends AbstractCommand
{
    /**
     * Вызов воркера из консоли
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('Фоновый обработчик сервиса пользовательского баланса запущен');
        try {
            $queue = new Queue();
            $queue->bindQueue(Config::getValue(Config::LISTEN_QUEUE))
                ->attachConsumer(function ($message) {
                    $connector = new Connector();
                    $connector->messageArrival($message);
                })
                ->work();
        } catch (\ErrorException $exception) {
            $this->logger->critical($exception->getMessage());
        }
    }

    /**
     * Настройка описания для комманды
     */
    protected function configure()
    {
        $this->setName('worker');
        $this->setDescription('Фоновый обработчик сервиса пользовательского баланса');
        $this->setHelp('Для работы запустите один или несколько процессов');
    }
}