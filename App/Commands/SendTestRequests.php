<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 12.09.17
 * Time: 0:37
 */

namespace App\Commands;


use App\Service\TestPerformer;
use Kernel\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendTestRequests extends AbstractCommand
{

    /**
     * Вызов комманды из консоли
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('Начинаем тестирование');
        $testPerformer = new TestPerformer();
        $testPerformer->perform();
        $this->logger->info('Все хорошо!');
    }

    /**
     * Настройка описания для комманды
     */
    protected function configure()
    {
        $this->setName('tester');
        $this->setDescription('Отправка тестовых данных в очередь, для проверки функциональности системы');
    }
}