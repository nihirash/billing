<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 18:31
 */

namespace App\Operation;


use App\Exception\FinanceException;
use App\Factory\ModelFactory;
use Entity\FrozenMoney;
use Kernel\AbstractOperation;

/**
 * Разблокирование замороженных средств - возвращаем пользователю
 *
 * @package App\Operation
 */
class UnblockMoney extends AbstractOperation
{
    /**
     * @var string  Имя операции
     */
    protected $name = 'Разблокирование средств';

    /**
     * Установить номер запроса на заморозку средств
     *
     * @param int $requestId
     * @return $this
     */
    public function setRequestId(int $requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * Выполнить операцию
     * @return mixed
     * @throws FinanceException
     */
    public function perform()
    {
        if (!$this->userId || !$this->requestId) {
            throw new FinanceException('Не достаточно данных для выполнения операции');
        }
        /**
         * @var FrozenMoney $frozenMoney
         */
        $frozenMoney = ModelFactory::FrozenMoney($this->requestId);
        if (!$frozenMoney) {
            throw  new FinanceException('Отсутствуют данные о заморозке денег');
        }

        if ($frozenMoney->getUserBalanceModel()->getUserId() != $this->userId) {
            throw new FinanceException('Замороженные деньги не принадлежат пользователю');
        }

        if (null !== $frozenMoney->getUnfrozenAt()) {
            throw new FinanceException('Деньги уже размороженны');
        }

        $this->amount = $frozenMoney->getAmount();
        $this->beginOperation();

        if ($this->userBalanceModel->getFrozen() < $this->amount) {
            $this->rollbackOperation();
            throw new FinanceException('Баланс пользователя не сходится.');
        }

        $newBalance = $this->userBalanceModel->getBalance() + $this->amount;
        $newFrozen = $this->userBalanceModel->getFrozen() - $this->amount;

        $this->userBalanceModel
            ->setBalance($newBalance)
            ->setFrozen($newFrozen)
            ->save();

        $frozenMoney
            ->setUnfrozenAt(new \DateTime())
            ->save();

        return ['financeLogEventId' => $this->finishOperation()];
    }
}