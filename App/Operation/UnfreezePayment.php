<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 21:03
 */

namespace App\Operation;


use App\Exception\FinanceException;
use App\Factory\ModelFactory;
use Entity\FrozenMoney;
use Kernel\AbstractOperation;

/**
 * Разморозка отложенного платежа
 *
 * @package App\Operation
 */
class UnfreezePayment extends AbstractOperation
{
    /**
     * @var string      Имя операции
     */
    protected $name = 'Разморозка платежа';

    /**
     * Выполнить операцию
     *
     * @return mixed
     * @throws FinanceException
     */
    public function perform()
    {
        if (!$this->userId || !$this->requestId) {
            throw new FinanceException('Не достаточно данных для выполнения операции');
        }
        /**
         * @var FrozenMoney $frozenMoney
         */
        $frozenMoney = ModelFactory::FrozenMoney($this->requestId);

        if (!$frozenMoney) {
            throw  new FinanceException('Отсутствуют данные о заморозке денег');
        }

        if ($frozenMoney->getUserBalanceModel()->getUserId() != $this->userId) {
            throw new FinanceException('Замороженные деньги не принадлежат пользователю');
        }

        if (null !== $frozenMoney->getUnfrozenAt()) {
            throw new FinanceException('Деньги уже размороженны');
        }

        $this->amount = $frozenMoney->getAmount();

        $this->beginOperation();
        if ($this->userBalanceModel->getFrozen() < $this->amount) {
            $this->rollbackOperation();
            throw new FinanceException('У пользователя не сходится баланс');
        }

        $frozenMoney
            ->setUnfrozenAt(new \DateTime())
            ->save();

        $newFreeze = $this->userBalanceModel->getFrozen() - $frozenMoney->getAmount();
        $this->userBalanceModel
            ->setFrozen($newFreeze)
            ->save();

        return ['financeLogEventId' => $this->finishOperation()];
    }
}