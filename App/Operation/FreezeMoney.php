<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 17:35
 */

namespace App\Operation;


use App\Exception\FinanceException;
use Entity\FrozenMoney;
use Kernel\AbstractOperation;

/**
 * Заморозка денег
 *
 * @package App\Operation
 */
class FreezeMoney extends AbstractOperation
{
    /**
     * @var string  Имя операции
     */
    protected $name = 'Заморозка средств';
    /**
     * @var bool    Отрицательное влияние на баланс?
     */
    protected $isNegative = true;

    /**
     * Выполнить операцию
     * @return mixed
     * @throws FinanceException
     */
    public function perform()
    {
        if (!$this->userId) {
            throw new FinanceException('Не достаточно данных для выполнения операции');
        }

        if ($this->amount < 1) {
            throw new FinanceException('Не возможно заморозить меньше одного цента');
        }

        $this->beginOperation();

        if ($this->userBalanceModel->getBalance() < $this->amount) {
            $this->rollbackOperation();
            throw new FinanceException('Недостаточно средств');
        }

        $newBalance = $this->userBalanceModel->getBalance() - $this->amount;
        $newFrozen = $this->userBalanceModel->getFrozen() + $this->amount;
        $frozenMoney = new FrozenMoney();

        $frozenMoney
            ->setUserBalanceModel($this->userBalanceModel)
            ->setFrozenAt(new \DateTime())
            ->setFinancelog($this->finLogModel)
            ->setAmount($this->amount)
            ->save();

        $this->userBalanceModel
            ->setBalance($newBalance)
            ->setFrozen($newFrozen);

        $this->finishOperation();

        return ['freezeMoneyRequestId' => $frozenMoney->getId()];
    }
}