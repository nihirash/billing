<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 21:26
 */

namespace App\Operation;


use Kernel\AbstractOperation;

/**
 * Перевод денег от пользователя к пользователю
 * Метаоперация - списываем деньги у пользователя 1,
 * если получилось - то отдаем пользователю 2.
 *
 * @package App\Operation
 */
class MoneyTransfer extends AbstractOperation
{
    /**
     * @var string  Имя операции
     */
    protected $name = 'Перевод денег';

    /**
     * Выполнить операцию
     *
     * @return mixed
     */
    public function perform()
    {
        /**
         * Операция синтетическая.
         * Снимаем деньги со своего счета, если все ок - то зачисляем другому пользователю
         */
        $pay = new Draw();
        $operationResult = $pay
            ->setUserId($this->userId)
            ->setAmount($this->amount)
            ->setName('Перечисление денег пользователю ' . $this->toUserId)
            ->perform();

        $addMoney = new AddMoney();
        $addMoney
            ->setName('Получение перевода от пользователя ' . $this->userId)
            ->setAmount($this->amount)
            ->setUserId($this->toUserId)
            ->perform();

        return $operationResult;
    }
}