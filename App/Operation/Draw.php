<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 16:01
 */

namespace App\Operation;


use App\Exception\FinanceException;
use Kernel\AbstractOperation;

/**
 * Списание денег с счета пользователя
 *
 * @package App\Operation
 */
class Draw extends AbstractOperation
{
    /**
     * @var string  Имя операции
     */
    protected $name = 'Оплата на сайте';
    /**
     * @var bool    Отрицательно влияет на баланс
     */
    protected $isNegative = true;

    /**
     * Выполнить операцию
     * @return mixed
     * @throws FinanceException
     */
    public function perform()
    {
        if (!$this->userId) {
            throw new FinanceException('Не достаточно данных для выполнения операции');
        }

        if ($this->amount < 1) {
            throw new FinanceException('Не возможно снять меньше одного цента');
        }

        $this->beginOperation();

        if ($this->userBalanceModel->getBalance() < $this->amount) {
            $this->rollbackOperation();
            throw  new FinanceException('Недостаточно средств');
        }

        $newBalance = $this->userBalanceModel->getBalance() - $this->amount;
        $this->userBalanceModel->setBalance($newBalance);

        return ['financeLogEventId' => $this->finishOperation()];
    }
}