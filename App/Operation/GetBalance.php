<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 22:11
 */

namespace App\Operation;


use App\Factory\ModelFactory;
use Kernel\AbstractOperation;

/**
 * Запрос баланса пользователя
 * @package App\Operation
 */
class GetBalance extends AbstractOperation
{
    /**
     * Выполнить операцию
     *
     * @return mixed
     */
    public function perform()
    {
        $userBalance = ModelFactory::getUserBalanceModel($this->userId);
        $balance = $userBalance->getBalance();
        $frozen = $userBalance->getFrozen();

        return [
            'balance' => $balance,
            'frozen' => $frozen
        ];
    }
}