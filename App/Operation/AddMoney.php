<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 15:34
 */

namespace App\Operation;


use App\Exception\FinanceException;
use Kernel\AbstractOperation;

/**
 * Внесение денег на счет пользователю
 *
 * @package App\Operation
 */
class AddMoney extends AbstractOperation
{
    /**
     * @var string  Имя операции
     */
    protected $name = 'Начисление на счет';

    /**
     * Выполнить операцию
     *
     * @return array
     * @throws FinanceException
     */
    public function perform()
    {
        if (!$this->userId) {
            throw new FinanceException('Не достаточно данных для выполнения операции');
        }

        if ($this->amount < 1) {
            throw new FinanceException('Не возможно совершить начисление суммы меньше одного цента');
        }

        $this->beginOperation();

        $newBalance = $this->userBalanceModel->getBalance() + $this->amount;

        $this->userBalanceModel
            ->setBalance($newBalance)
            ->save();

        return ['financeLogEventId' => $this->finishOperation()];
    }
}