<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 17:57
 */

namespace App;


class Config
{
    // Константы полей конфига

    // Свойства конфига
    const REDIS = 'redis';          // Сервисы
    const RABBITMQ = 'rabbitmq';    // -""-
    const DOCTRINE = 'doctrine';    // -""-
    const LISTEN_QUEUE = 'listenQueue'; // Очередь, которую слушаем
    // Поля свойств
    const HOST = 'host';
    const PORT = 'port';
    const SCHEME = 'scheme';
    const USER = 'user';
    const PASSWORD = 'password';
    const DRIVER = 'driver';
    const DBNAME = 'dbname';

    // Операции над балансом
    const FREEZE = 'freeze';     // Заморозка денег
    const ADD = 'add';           // Зачисление на счет
    const UNFREEZE = 'unfreeze'; // Разморозка денег
    const DRAW = 'draw';         // Списание денег
    const TRANSFER = 'transfer'; // Перевод денег
    const UNBLOCK = 'unblock';   // Разблокировка
    const GET = 'get';           // Получение баланса

    /**
     * @var array   Конфиг нашего приложения
     */
    private static $config = [
        self::REDIS => [
            self::SCHEME => 'tcp',
            self::HOST => 'redis',
            self::PORT => 6379,
        ],
        self::RABBITMQ => [
            self::HOST => 'rabbitmq',
            self::PORT => 5672,
            self::USER => 'rabbit',
            self::PASSWORD => 'develop',
        ],
        self::DOCTRINE => [
            self::DRIVER => 'pdo_mysql',
            self::HOST => 'mysql',
            self::USER => 'develop',
            self::PASSWORD => 'develop',
            self::DBNAME => 'balance'
        ],
        self::LISTEN_QUEUE => 'balance',
    ];

    /**
     * Получить запись из лога
     *
     * @param string $field Поле в конфиге
     * @return mixed            Данные из конфига
     *
     * @throws \Exception
     */
    public static function getValue(string $field)
    {
        if (!isset(self::$config[$field])) {
            throw new \Exception('Нет записи в конфиге');
        }

        return self::$config[$field];
    }
}