<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 15:18
 */

namespace App\Factory;


use Entity\FinanceLog;
use Entity\FrozenMoney;
use Entity\UserBalance;
use Kernel\Bootstrap;

class ModelFactory
{
    /**
     * Получить модель пользовательского баланса(если записи нет - будет создана)
     *
     * @param int $userId
     * @return UserBalance
     */
    public static function getUserBalanceModel(int $userId): UserBalance
    {
        $entityManager = Bootstrap::getEntityManager();
        $model = $entityManager->getRepository(UserBalance::class)
            ->findOneBy(['userId' => $userId]);

        if (null === $model) {
            $model = new UserBalance();
            $model->setUserId($userId)
                ->setBalance(0)
                ->setFrozen(0)
                ->save();
        }

        return $model;
    }

    /**
     * Получить запись о замороженных средствах
     *
     * @param int $id id-записи
     * @return null|FrozenMoney     Может вернуть или null, если записи нет, или модельку
     */
    public static function FrozenMoney(int $id)
    {
        $entityManager = Bootstrap::getEntityManager();
        $model = $entityManager->getRepository(FrozenMoney::class)
            ->findOneBy(['id' => $id]);

        return $model;
    }

    /**
     * Получить объект фин. лога
     *
     * @param int $id
     * @return null|FinanceLog
     */
    public static function getFinanceLog(int $id)
    {
        $entityManager = Bootstrap::getEntityManager();
        $model = $entityManager->getRepository(FinanceLog::class)
            ->findOneBy(['id' => $id]);

        return $model;
    }
}