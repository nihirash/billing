<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 21:39
 */

namespace App\Factory;


use App\Config;
use App\Exception\LogicException;
use App\Operation\AddMoney;
use App\Operation\Draw;
use App\Operation\FreezeMoney;
use App\Operation\GetBalance;
use App\Operation\MoneyTransfer;
use App\Operation\UnblockMoney;
use App\Operation\UnfreezePayment;
use Kernel\AbstractOperation;

class OperationFactory
{
    /**
     * @var array   Массив операций
     */
    private static $operations = [
        Config::GET => GetBalance::class,
        Config::ADD => AddMoney::class,
        Config::DRAW => Draw::class,
        Config::TRANSFER => MoneyTransfer::class,
        Config::FREEZE => FreezeMoney::class,
        Config::UNBLOCK => UnblockMoney::class,
        Config::UNFREEZE => UnfreezePayment::class
    ];

    /**
     * Получить класс операции
     *
     * @param string $operationConstant Константа операции
     * @return AbstractOperation        Класс операции
     * @throws LogicException
     */
    public static function getOperationClass(string $operationConstant): AbstractOperation
    {
        if (!isset(self::$operations[$operationConstant])) {
            throw new LogicException('Операция не зарегистрирована');
        }

        return new self::$operations[$operationConstant];
    }
}