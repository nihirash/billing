<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 0:39
 */

namespace Entity;

use Kernel\AbstractModel;

/**
 * Табличка замороженных денег
 * @package Entity
 *
 * @Entity
 * @Table(name="frozen_money")
 */
class FrozenMoney extends AbstractModel
{
    /**
     * @Id
     * @Column(type="integer", name="id")
     * @GeneratedValue
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="UserBalance")
     * @JoinColumn(name="user_id", referencedColumnName="user_id", nullable=false)
     */
    private $userBalanceModel;
    /**
     * @Column(type="datetime", name="frozen_ts")
     */
    private $frozenAt;
    /**
     * @Column(type="bigint", name="amount")
     */
    private $amount;
    /**
     * @Column(type="datetime", name="unfrozen_ts", nullable=true)
     */
    private $unfrozenAt;
    /**
     * @ManyToOne(targetEntity="FinanceLog")
     * @JoinColumn(name="financelog_id", referencedColumnName="id")
     */
    private $financelog;

    /**
     * Получить id-записи
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * user к которому относится запись
     *
     * @return UserBalance
     */
    public function getUserBalanceModel(): UserBalance
    {
        return $this->userBalanceModel;
    }

    /**
     * Установить userId у записи
     *
     * @param UserBalance|int $userBalanceModel
     * @return FrozenMoney
     */
    public function setUserBalanceModel(UserBalance $userBalanceModel): FrozenMoney
    {
        $this->userBalanceModel = $userBalanceModel;
        return $this;
    }

    /**
     * Замороженная сумма
     *
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function setAmount(int $amount): FrozenMoney
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Запись в фин.логе
     *
     * @return FinanceLog
     */
    public function getFinancelog(): FinanceLog
    {
        return $this->financelog;
    }

    /**
     * Привязать запись в фин.логе
     *
     * @param FinanceLog $financelog
     * @return $this
     */
    public function setFinancelog(FinanceLog $financelog): FrozenMoney
    {
        $this->financelog = $financelog;
        return $this;
    }

    /**
     * Дата заморозки
     *
     * @return \DateTime
     */
    public function getFrozenAt(): \DateTime
    {
        return $this->frozenAt;
    }

    /**
     * Установить дату заморозки денег
     *
     * @param \DateTime $frozenAt
     * @return $this
     */
    public function setFrozenAt(\DateTime $frozenAt): FrozenMoney
    {
        $this->frozenAt = $frozenAt;
        return $this;
    }

    /**
     * Дата разморозки
     *
     * @return \DateTime|null
     */
    public function getUnfrozenAt()
    {
        return $this->unfrozenAt;
    }

    /**
     * Устновить дату разморозки денежных средств
     *
     * @param \DateTime $unfrozenAt
     * @return $this
     */
    public function setUnfrozenAt(\DateTime $unfrozenAt): FrozenMoney
    {
        $this->unfrozenAt = $unfrozenAt;
        return $this;
    }
}