<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 0:46
 */

namespace Entity;

use Kernel\AbstractModel;

/**
 * Таблица финансового лога
 * @package Entity
 *
 * @Entity
 * @Table(name="finance_log")
 */
class FinanceLog extends AbstractModel
{
    /**
     * @Id
     * @Column(name="id", type="bigint")
     * @GeneratedValue
     */
    private $id;
    /**
     * @ManyToOne(targetEntity="UserBalance")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    private $userBalanceModel;
    /**
     * @Column(type="bigint", name="balance_before")
     */
    private $balanceBefore;
    /**
     * @Column(type="bigint", name="amount")
     */
    private $amount;
    /**
     * @Column(type="string", name="log")
     */
    private $log;
    /**
     * @Column(type="boolean", name="completed")
     */
    private $completed;

    /**
     * Получить id записи
     *
     * @return int
     */
    public function getId(): int
    {
        return intval($this->id);
    }

    /**
     * Получить баланс до операции
     *
     * @return int
     */
    public function getBalanceBefore(): int
    {
        return intval($this->balanceBefore);
    }

    /**
     * Устновить баланс пользователя до операции
     *
     * @param int $balanceBefore
     * @return FinanceLog
     */
    public function setBalanceBefore(int $balanceBefore): FinanceLog
    {
        $this->balanceBefore = $balanceBefore;
        return $this;
    }

    /**
     * Получить сумму операции
     *
     * @return int
     */
    public function getAmount(): int
    {
        return intval($this->amount);
    }

    /**
     * Установить сумму операции
     *
     * @param int $amount
     * @return FinanceLog
     */
    public function setAmount(int $amount): FinanceLog
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Получить строку лога
     *
     * @return string
     */
    public function getLog(): string
    {
        return $this->log;
    }

    /**
     * Выставить запись в фин логе
     *
     * @param string $log
     * @return FinanceLog
     */
    public function setLog(string $log): FinanceLog
    {
        $this->log = $log;
        return $this;
    }

    /**
     * Операция завершена?
     *
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * Установить флаг завершенности операции
     *
     * @param bool $completed
     * @return FinanceLog
     */
    public function setCompleted(bool $completed): FinanceLog
    {
        $this->completed = $completed;
        return $this;
    }

    /**
     * Получить модельку пользовательского баланса
     *
     * @return UserBalance
     */
    public function getUserBalanceModel(): UserBalance
    {
        return $this->userBalanceModel;
    }

    /**
     * Установить модельку пользователя, к которому относиться запись в финлоге
     *
     * @param UserBalance $userBalanceModel
     * @return $this
     */
    public function setUserBalanceModel(UserBalance $userBalanceModel)
    {
        $this->userBalanceModel = $userBalanceModel;
        return $this;
    }


}