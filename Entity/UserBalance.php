<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 0:34
 */

namespace Entity;

use Kernel\AbstractModel;

/**
 * Таблица баланса пользователей
 * @package Entity
 *
 * @Entity
 * @Table(name="user_balance")
 */
class UserBalance extends AbstractModel
{
    /**
     * @Id
     * @Column(type="bigint", name="user_id")
     */
    private $userId;
    /**
     * @Column(type="bigint", name="balance")
     */
    private $balance;
    /**
     * @Column(type="bigint", name="frozen")
     */
    private $frozen;

    /**
     * Получить userId
     *
     * @return int
     */
    public function getUserId(): int
    {
        return intval($this->userId);
    }

    /**
     * Установить userId
     *
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): UserBalance
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Получить баланс пользователя
     *
     * @return int
     */
    public function getBalance(): int
    {
        return intval($this->balance);
    }

    /**
     * Установить баланс пользователю
     *
     * @param mixed $balance
     * @return $this
     */
    public function setBalance(int $balance): UserBalance
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * Получить количество замороженных денег
     *
     * @return int
     */
    public function getFrozen(): int
    {
        return intval($this->frozen);
    }

    /**
     * Установить количество замороженных денег
     *
     * @param int $frozen
     * @return $this
     */
    public function setFrozen(int $frozen): UserBalance
    {
        $this->frozen = $frozen;
        return $this;
    }

}