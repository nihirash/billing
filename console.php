<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 10.09.17
 * Time: 15:28
 */

require_once 'vendor/autoload.php';

define('BASE_PATH', __DIR__);

echo "Ожидание 30 секунд - инициализация окружения" . PHP_EOL;

sleep (30);

(new Kernel\Bootstrap())
    ->consoleApp();