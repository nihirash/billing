<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 11.09.17
 * Time: 0:23
 */

require_once 'vendor/autoload.php';

define('BASE_PATH', __DIR__);

(new Kernel\Bootstrap());

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(
    \Kernel\Bootstrap::getEntityManager());

